# VentureBot-Bridge
VentureBot-Node is the NodeJS version of [VentureBot](https://gitlab.com/VentureTowny/VentureBot/)

## Website
https://dynamicdonkey.github.io

## Dependencies
This bot uses DiscordJS to attach to the Discord server.

## Releases
There will be no releases. This is only here as an archive.

## Running
VentureBot-Node uses NodeJS and npm to run
```
$ npm start
``` 

## Contributing
You have an optimization or a new idea you want to implement? Don't hesitate to open a Pull Request against the `dev` branch

