const config = require("./config.json");
const Discord = require('discord.js');
//const swearModule = require("./modules/swear.js");

var colors = require('colors');

const client = new Discord.Client();
const kiwiDetector = ["Kiwi", "Aussie", "kiwi", "aussie"]
const ilyVenture = ["ily Venture"]

const ver = "1.0.1-VN"

const changelog = "Complete rework for a new server, VentureNetwork."

var invite = "https://discord.gg/58xRVm6";

//var bypass = false;
//var swear = false;

client.on('ready', () => {
  //console.log('\n[INFO] Logged in as VentureBot#7737.\n'.teal);
  //client.user.setStatus("Online")
  //client.user.setGame('VB | /vn help')
  //client.user.setGame('Fortnite', "https://twitch.tv/DynamicDonkey")
  //client.user.setActivity({game: {name: "VB | /vn help", type: 1}});
  client.user.setActivity('VN | /vn help', { type: 'PLAYING' });
  //swearModule.bypassRoutine();
  //swearModule.swearRoutine();
});




client.on('message', msg => {
  if (msg.content === '/vn help') {
     msg.channel.send({embed: {
            color: 5592575,
            author: {
              name: client.user.username,
              icon_url: client.user.avatarURL
            },
            title: "Here is a list of commands.",
            description: "Need some help? Here you go!",
            fields: [{
                name: "/vn help",
                value: "Displays this dialog.",
                color: "16098851"
              },
              {
                name: "/vn staff",
                value: "Gives you a list of the staff members Minecraft and Discord names."
              },
              {
                name: "/vn ip",
                value: "Gives the server address."
              },
              {
                name: "/vn discord",
                value: "Creates a shareable Discord server link."
              },
              {
                name: "/vn sub",
                value: "Gives you a link to the subreddit."
              },
              {
                name: "/vn rules",
                value: "Tells the user to visit #guidelines."
              },
              {
                "name": "/vn donate",
                "value": "Prints a link to the Buycraft."
              },
              {
                "name": "/vn info",
                "value": "Prints information about the bot."
              },
	      {
		"name": "/vn source",
		"value": "Prints a link to our source code repositories."
	      },	
            ],
            timestamp: new Date(),
            footer: {
              icon_url: client.user.avatarURL,
              text: "© DynamicDonkey#6339"
            }
          }
        });
    }
});

client.on('message', msg => {
  if (msg.content === '/vn staff') {
    msg.channel.send({embed: {
        color: 5592575,
    author: {
      name: client.user.username,
      icon_url: client.user.avatarURL
    },
    title: "VentureNetwork Staff",
    description: "Use these to get in contact with us!",
    fields: [{
        name: "[OWNER] CantAim_",
        value: "Our beloved owner and founder of the server. Be sure to thank him!"
      },
      {
        name: "[DEV] Dynamic_Donkey",
        value: "Developer of this bot, manager of the subreddit, and misc. tech work for the server."
      }
    ],
    timestamp: new Date(),
    footer: {
      icon_url: client.user.avatarURL,
      text: "© DynamicDonkey#6339"
    }
  }
});
  }
});

client.on('message', msg => {
  if (msg.content === '/vn ip') {
    msg.channel.send({embed: {
        color: 5592575,
        author: {
          name: client.user.username,
          icon_url: client.user.avatarURL
        },
        title: "VentureNetwork Server IP",
        description: "Share this with your friends so they can join!",
        fields: [{
            name: "Server Address",
            value: "`mc.venturenetwork.us`"
          }
        ],
        timestamp: new Date(),
        footer: {
          icon_url: client.user.avatarURL,
          text: "© DynamicDonkey#6339"
        }
      }
    });
    }
  });

client.on('message', msg => {
  if (msg.content === '/vn discord') {
    msg.channel.send(invite);
  }
});

client.on('message', msg => {
  if (msg.content === '/vn sub') {
    msg.channel.send('https://www.reddit.com/r/VentureNetwork/');
  }
});

//if(swear) {
//  client.on('message', msg => {
//      if( swearWords.some(word => msg.content.includes(word)) ) {
//         msg.reply("Let's keep swearing to a minimum. Okay?");
//      }  
//  });
//} else if(!swear) {
//  client.on('message', msg => {
//    if(swearWords.some(word => msg.content.includes(word)) ) {
//      console.log("[WARN] User has used a swear word, but module is disabled.".yellow);
//    }
//  });
//  console.log("\n[INFO] Swear module is disabled.".cyan)
//}

//if(bypass) {
//  client.on('message', msg => {
//      if( swearBypass.some(word => msg.content.includes(word)) ) {
//         msg.reply("Really? Trying to bypass? C'mon...");
//      }  
//  });
//} else if(!bypass) {
//  client.on('message', msg => {
//    if(swearBypass.some(word => msg.content.includes(word)) ) {
//      console.log("[WARN] User has bypassed swear list, but module is disabled.".yellow);
//    }
//  });
//  console.log("[INFO] Bypass module is disabled.\n\n".cyan)
//}

client.on('message', msg => {
    if(kiwiDetector.some(word => msg.content.includes(word))){
        msg.channel.send("G'day mate!");
    }
});

client.on('message', msg => {
  if(ilyVenture.some(word => msg.content.includes(word))){
      msg.reply("awww ily too <3");
  }
});

client.on('message', msg => {
    if (msg.content === '/vn rules') {
       msg.channel.send({embed: {
              color: 5592575,
              author: {
                name: client.user.username,
                icon_url: client.user.avatarURL
              },
              title: "Here are the rules, as requested!",
              description: "Now whaddya say we follow them?",
              fields: [{
                  name: "VV Go to that channel",
                  value: "#guidelines - The channel that has all of the rules!",
                  color: "16098851"
                },
              ],
              timestamp: new Date(),
              footer: {
                icon_url: client.user.avatarURL,
                text: "© DynamicDonkey#6339"
              }
            }
          });
      }
  });

/*
  client.on('message', msg => {
    if (msg.content === '/vn event sg') {
      msg.delete();
      msg.channel.send({embed: {
          "title": "Survival Games Event",
          "description": "In-Game event. Survival games competition.",
          "color": 3589829,
          "timestamp": new Date(),
          "footer": {
            "icon_url": client.user.avatarURL,
            "text": "© DynamicDonkey#6339, Gold#6848"
          },
          "thumbnail": {
            "url": "https://i.imgur.com/SZ1a5h8.png"
          },
          "image": {
            "url": "https://s3.amazonaws.com/files.enjin.com/381916/modules/forum/attachments/2014-04-17_16.14.14_1397764858.png"
          },
          "author": {
            "name": client.user.username,
            "icon_url": client.user.avatarURL
          },
          "fields": [
            {
              "name": "Rules",
              "value": "Find loot scattered around the map and use it to dominate the competition!"
            },
            {
              "name": "Date",
              "value": "xx/xx/18, 4:00 CST"
            },
            {
              "name": "Map",
              "value": "Map name"
            },
            {
              "name": "Prize",
              "value": "How does a Venture Key soung? Pretty good, I would hope."
            },
            {
              "name": "Specifics",
              "value": "Any use of cheating, hacking, or external client modifications will result in an instant disqualification and investigation.",
            }
          ],
        }
      });
    }
});
*/

client.on('message', msg => {
  if (msg.content === '/vn donate') {
    msg.channel.send({embed: {
        color: 5592575,
        author: {
          name: client.user.username,
          icon_url: client.user.avatarURL
        },
        title: "VentureNetwork Donation",
        description: "Want to help us out? Click the link below to donate!",
        fields: [{
            name: "Donation link",
            value: "http://venturenetwork.buycraft.net/"
          }
        ],
        timestamp: new Date(),
        footer: {
          icon_url: client.user.avatarURL,
          text: "© DynamicDonkey#6339"
        }
      }
    });
    }
  });

  client.on('message', msg => {
    if (msg.content === '/vn info') {
      msg.channel.send({embed: {
          color: 5592575,
          author: {
            name: client.user.username,
            icon_url: client.user.avatarURL
          },
          title: "VentureBot Info",
          description: "Got the details here for ya, boss!",
          fields: [{
              name: "Developer",
              value: "Developed by DynamicDonkey#6339"
            },
            {
              "name": "Version",
              "value": ver
            },
            {
              "name": "Dev info",
              "value": "Developed using `discord.js`\nand `colors.js`"
            },
            {
              "name": "Changelog",
              "value": changelog
            }
            
            
          ],
          timestamp: new Date(),
          footer: {
            icon_url: client.user.avatarURL,
            text: "© DynamicDonkey#6339"
          }
        }
      });
      }
    });

client.on('message', msg => {
  if (msg.content === '/vn source') {
    msg.channel.send({embed: {
        color: 5592575,
        author: {
          name: client.user.username,
          icon_url: client.user.avatarURL
        },
        title: "Source code/projects",
        description: "https://gitlab.com/VentureNetwork",
        fields: [{
            name: "Projects",
            value: "[VentureNetwork/VentureWebsite](https://gitlab.com/VentureNetwork/VentureWebsite)\n[VentureNetwork/VentureBot](https://gitlab.com/VentureNetwork/VentureBot)\n[DynamicDonkey/VentureBot-Node](https://gitlab.com/DynamicDonkey/VentureBot-Node)"
          }
        ],
        timestamp: new Date(),
        footer: {
          icon_url: client.user.avatarURL,
          text: "© DynamicDonkey#6339"
        }
      }
    });
    }
  });


client.login(config.token);
